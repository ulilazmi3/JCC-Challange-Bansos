# JCC Challange Bansos

Project 3 Pendataan Bansos (Frontend JS Project)
Candradimuka Jabar Coding Camp

## Informasi nama peserta, kelas belajar.

Nama :  Muhammad Ulil 'Azmi

Kelas : Back-End Web: Golang

## Link video/record screen demo yang berisi penjelasan singkat hasil pengujian aplikasi (pastikan bisa dibuka tanpa login).
https://drive.google.com/file/d/1ZzBOnkOnMZdqwgEBbtlGGnzsKkawdvpb/view?usp=sharing

## Link deploy di hosting (Github Pages/Netlify/Surge/Render).
https://lucid-jang-01350e.netlify.app/
