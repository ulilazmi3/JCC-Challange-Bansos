import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
          light: {
            primary: '#1E88E5',
            secondary: '#FFD026',
            accent: '#16A75C',
            error: '#DD5E5E',
            info: '#2196F3',
            success: '#20A95A',
            warning: '#FED32C',
          },
        },
      },
});
